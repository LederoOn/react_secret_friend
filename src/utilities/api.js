import axios from "axios"

const api = axios.create({ baseURL: "https://immense-mountain-51308.herokuapp.com/"})

export default api