import { useState } from "react";
import ListFriends from "./components/ListFriends";
import RegisterFriend from "./components/RegisterFriend";
import UserMenu from "./components/UserMenu";
import { MainContainer } from './styles'



function App() {
  const [ currentTable, setCurrentTable ] = useState(1)
  return (
    <MainContainer>
    <h1>Seu Amigo Secreto!</h1>
    <UserMenu handleMenu={setCurrentTable} color={currentTable}/>
    <div>
      {currentTable === 1 && <RegisterFriend/> }
      {currentTable === 2 && <ListFriends/> }
    </div>
    </MainContainer>
  );
}

export default App;
