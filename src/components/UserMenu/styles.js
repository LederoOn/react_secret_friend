import styled from 'styled-components'


export const MainContainer = styled.div`
    display: flex;
    justify-content: center;
    padding: 20px;
`

const button = styled.button`   
    border: none;
    cursor: pointer;
    height: 40px;
    transition: 0.2s;

    &:nth-child(1) {
        padding-right: 20%;
        padding-left: 4%;
        border-bottom-left-radius: 10px;
        border-top-left-radius: 10px;
        background: linear-gradient(to left, var(--colorTwo), var(--colorThree), var(--colorThree));
    }

    &:nth-child(2) {
        padding-left: 20%;
        padding-right: 4%;
        border-bottom-right-radius: 10px;
        border-top-right-radius: 10px;
        background: linear-gradient(to right, var(--colorTwo), var(--colorThree), var(--colorThree));
    }
`

export const StyledButton1 = styled(button)`
    background: ${props => props.color === 1 ? "var(--colorTwo) !important" : "var(--colorThree)"};
    
    &:hover {
        color: ${props => props.color === 1 ? "black" : "var(--colorTwo)"};
        border-left: ${props => props.color === 1 ? "none" : "2px solid var(--colorTwo)"};
        transition: 0.2s;
    }
`
export const StyledButton2 = styled(button)`
    background: ${props => props.color === 2 ? "var(--colorTwo) !important" : "var(--colorThree)"};
    
    &:hover {
        color: ${props => props.color === 2 ? "black" : "var(--colorTwo)"};
        border-right: ${props => props.color === 2 ? "none" : "2px solid var(--colorTwo)"};
        transition: 0.2s;
    }
`