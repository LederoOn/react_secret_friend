import { MainContainer, StyledButton1, StyledButton2 } from "./styles";

const UserMenu = ({handleMenu, color}) => {
    return <MainContainer>
        <StyledButton1 color={color} onClick={() => handleMenu(1)}>Registre-se!</StyledButton1>
        <StyledButton2 color={color} onClick={() => handleMenu(2)}>Lista de Amigos!</StyledButton2>
    </MainContainer>
};

export default UserMenu;