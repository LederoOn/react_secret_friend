import { useState } from "react";
import api from '../../utilities/api'
import { StyledDiv, StyledButton } from './styles'

const RegisterFriend = () => {
    const [newUser, setNewUser] = useState({name: "", email: ""})
    const [error, setError] = useState(false)
    const [sucess, setSucess] = useState(false)

    const register = () => newUser.name === "" || newUser.email === "" ? null : api.post("/friends", newUser).then(res => setSucess(true)).catch(e => setError(true))

    return <StyledDiv>
        <span>Digite seu nome e seu email abaixo, e fique preparado para ser escolhido como o amigo secreto de alguém!</span>
        <input value={newUser.name} placeholder="Nome" onChange={(e) => setNewUser({...newUser, name: e.target.value})}/>
        <input value={newUser.email} placeholder="Email" onChange={(e) => setNewUser({...newUser, email: e.target.value})}/>
        <StyledButton onClick={register}>Registre-se!</StyledButton>
        { error && <span style={{color: "red"}}>Algo deu errado!</span>}
        { sucess && <span style={{color: "green"}}>Você está cadastrado!</span>}
    </StyledDiv>
};

export default RegisterFriend;