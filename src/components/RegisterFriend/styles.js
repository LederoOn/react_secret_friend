import styled from 'styled-components'


export const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 2px solid var(--colorTwo);
    box-shadow: 0px 0px 10px rgb(50, 50, 50);
    margin: 25px 25px 25px 25px;
    border-radius: 10px;
    width: 50%;
    margin: auto;

    span {
        width: 50%;
        margin: 10px;
    }

    input {
        margin: 4px;
        border-radius: 10px;
        padding-left: 10px;
        height: 40px;
        width: 220px;
    }
`
export const StyledButton = styled.button`
    width: 120px;
    height: 60px;
    font-size: 1.2rem;
    cursor: pointer;
    margin: 4px;
    border-radius: 10px;
    background-color: ${props => props.editing ? "var(--colorTwo)" : "var(--colorThree)"};
    transition: 0.5s;
    margin: 10px;
    border: 2px solid var(--colorTwo);

    &:hover {
        background-color: var(--colorTwo);
        transition: 0.5s;
    }
`