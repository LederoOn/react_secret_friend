import api from '../../utilities/api'
import { useEffect, useState } from 'react'
import Friend from '../Friend'
import { MainContainer, StyledButton } from './styles'

const ListFriends = () => {
    const [allFriends, setAllFriends] = useState([])
    const [result, setResult] = useState(false)
    const [loading, setLoading] = useState(false)
    const handleRefresh = () => api.get("/friends").then(res => setAllFriends(res.data))
    
    const handleEdit = async (id, itens)  => {await api.patch(`/friends/:?id=${id}`, itens).then(res => console.log(res)).catch(e => console.log(e)); handleRefresh()}
    const handleDelete = async (id) => {await api.delete(`/friends/:?id=${id}`); handleRefresh()}
    const handleChoose = () => {setResult(false); setLoading(true); api.post("/friends/send", allFriends[Math.floor(Math.random() * allFriends.length)]).then(res => setResult(res.data)).catch(err => console.log(err))}

    useEffect(() => {
        handleRefresh()
    }, [])

    return <MainContainer>
        <StyledButton onClick={handleChoose}>Realizar Sorteio!</StyledButton>
        { loading && <div>
            {result ? <a target="_blank" rel="noreferrer" href={result.link}>Confira o Email aqui!</a> : <img src="https://c.tenor.com/5o2p0tH5LFQAAAAi/hug.gif" alt="Loading"/>}
            </div>}
        {allFriends?.map(obj => <Friend key={obj.id} handleEdit={handleEdit} handleDelete={handleDelete} friend={obj}/>)}
    </MainContainer>
};

export default ListFriends;