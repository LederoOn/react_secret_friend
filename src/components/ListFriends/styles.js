import styled from 'styled-components'


export const MainContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    overflow: auto;
    height: 70vh;

    a {
        color: black;
        margin: 10px 20px 0px 20px;
    }

    img {
        width: 20px;
    }
`
export const StyledButton = styled.button`
    width: 220px;
    padding: 20px;
    font-size: 1.2rem;
    cursor: pointer;
    margin: 10px;
    border-radius: 10px;
    background-color: var(--colorThree);
    transition: 0.5s;
    border: 2px solid var(--colorTwo);

    &:hover {
        background-color: var(--colorTwo);
        transition: 0.5s;
    }
`