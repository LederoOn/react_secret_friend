import { useState } from 'react';
import { StyledDiv, EditingArea, Breaker, StyledButton} from './styles'

const Friend = ({friend, handleDelete, handleEdit}) => {
    const [editing, setEditing] = useState(false)
    const [data, setData] = useState({name: "", email: ""})

    return <StyledDiv>
            <h1>{friend.name}</h1>
            <p>{friend.email}</p>
        <Breaker>
            <StyledButton onClick={() => handleDelete(friend.id)}>Apagar</StyledButton>
            <StyledButton editing={editing} onClick={() => setEditing(!editing)}>Editar</StyledButton>
        </Breaker>
        
        {editing && <EditingArea>
                    <input placeholder="Novo Nome" value={data.name} onChange={(e) => setData({...data, name: e.target.value})}/>
                    <input placeholder="Novo Email" value={data.email} onChange={(e) => setData({...data, email: e.target.value})}/>
                    <StyledButton onClick={() => handleEdit(friend.id, data)}>Atualizar!</StyledButton>
                </EditingArea>}
    </StyledDiv>
};

export default Friend;