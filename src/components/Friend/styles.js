import styled from 'styled-components'


export const StyledDiv = styled.div`
    display: flex;
    flex-direction: column;
    border: 2px solid var(--colorTwo);
    box-shadow: 0px 0px 10px rgb(50, 50, 50);
    margin: 25px 25px 25px 25px;
    border-radius: 10px;
    width: 60%;

    input {
        margin: 4px;
        border-radius: 10px;
        padding-left: 10px;
    }
`

export const Breaker = styled.div`
    display: flex;
    justify-content: space-evenly;
`

export const EditingArea = styled.div``

export const StyledButton = styled.button`
    width: 100px;
    font-size: 1rem;
    cursor: pointer;
    margin: 4px;
    border-radius: 10px;
    background-color: ${props => props.editing ? "var(--colorTwo)" : "var(--colorThree)"};
    transition: 0.5s;

    &:hover {
        background-color: var(--colorTwo);
        transition: 0.5s;
    }
`