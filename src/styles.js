import styled from 'styled-components'

export const MainContainer = styled.div`
    width: 80vw;
    
    h1 {
        margin-top: 20px;
    }
`