import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
    :root {
        --colorOne: rgb(0, 150, 150);
        --colorTwo: rgb(100, 200, 100);
        --colorThree: rgb(0, 100, 100);
    }

    body {
        display: flex;
        justify-content: center;
        text-align: center;
        height: 100vh;
        background-color: var(--colorOne);

        h1 {
            font-size: 2rem;
        }
    }

    * {
        margin: 0;
        padding: 0;
        font-family: 'Roboto', sans-serif;
        border: none;
        user-select: none;

        ::-webkit-scrollbar {
        width: 10px;
        }

        ::-webkit-scrollbar-track {
            background: #f1f1f1;
        }

        ::-webkit-scrollbar-thumb {
            background: var(--colorThree);
        }

        ::-webkit-scrollbar-thumb:hover {
            background: var(--colorTwo);

        }
    }
`

export default GlobalStyle